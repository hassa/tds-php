<?php
    /** @var Utilisateur[] $utilisateurs */
    use App\Covoiturage\Modele\DataObject\Utilisateur;
    use App\Covoiturage\Lib\ConnexionUtilisateur;



    foreach ($utilisateurs as $utilisateur) {
        if(ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {

            echo "<p> Utilisateur de login <a href=\"?action=afficherDetail&login=" . rawurlencode($utilisateur->getLogin()) . "\">" . htmlspecialchars($utilisateur->getLogin()) . "</a> | " . "<a href=\"?action=supprimer&login=" . rawurlencode($utilisateur->getLogin()) . "\">supprimer</a> | " . "<a href=\"?action=afficherFormulaireMiseAJour&login=" . rawurlencode($utilisateur->getLogin()) . "\">modifier</a>" . "</p><br>";

        }
        else{
            echo "<p>Utilisateur de login <a href=\"?action=afficherDetail&login=" . rawurlencode($utilisateur->getLogin()) . "\">" . htmlspecialchars($utilisateur->getLogin()) . "</a></p><br>";
        }
    }
    echo "<a href=\"?action=afficherFormulaireCreation\">Créer un utilisateur</a>";
?>