<?php
    /** @var Utilisateur $utilisateur */
    use App\Covoiturage\Modele\DataObject\Utilisateur;
?>

<form method="get" action="">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input readonly value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" class="InputAddOn-field" type="text" placeholder="leblancj" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <input value="<?= htmlspecialchars($utilisateur->getNom()) ?>" class="InputAddOn-field" type="text" placeholder="leblanc" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <input value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" class="InputAddOn-field" type="text" placeholder="juste" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="amdp_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="amdp" id="amdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
        <input type='hidden' name='action' value='mettreAJour'>
    </fieldset>
</form>