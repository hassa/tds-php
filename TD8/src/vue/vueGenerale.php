<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <title><?php /**
             * @var string $titre
             */echo $titre; ?></title>
        <link rel="stylesheet" href="../ressources/style.css">
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference">Preference
                        <img src = "../../ressources/images/icone.jpg" alt="Icone" style="width: 32px; height: 32px;">
                        </a>
                    </li>
                    <?php
                    use App\Covoiturage\Lib\ConnexionUtilisateur;


                    if (!ConnexionUtilisateur::estConnecte()) {
                    ?>

                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur">
                            <img src = "../ressources/images/icone.jpg" style="width: 32px; height: 32px;" >
                        </a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur">
                            <img src = "../ressources/images/enter.png" style="width: 32px; height: 32px;" >
                        </a>
                    </li>
                        <li>
                            <a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?php echo rawurlencode(\App\Covoiturage\Lib\ConnexionUtilisateur::getLoginUtilisateurConnecte()); ?>">détail</a>
                        </li>

                        <?php
                    }
                    else{
                        ?>
                        <li>
                            <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur">
                                <img src = "../ressources/images/logout.png" style="width: 32px; height: 32px;" >
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>

            </nav>

        </header>
        <main>
            <?php /**
             * @var string $cheminCorpsVue
             */

                require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de ...
            </p>
        </footer>
    </body>
</html>