<form method="get" action="">
    <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur">
            <label for="utilisateurId">Utilisateur</label>
        </p>
        <p>
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet">
            <label for="trajetId">Trajet</label>
        </p>

        <input type='hidden' name='action' value='enregistrerPreference'>

    </fieldset>
</form>