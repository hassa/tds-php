<?php
    /** @var Trajet[] $trajets */
    use App\Covoiturage\Modele\DataObject\Trajet;
    foreach ($trajets as $trajet) {
        echo "<p> trajet " . "<a href=\"?action=afficherDetail&controleur=trajet&id=" . rawurlencode($trajet->getId()) . "\">" . htmlspecialchars($trajet->getId()) . "</a>" . " " . htmlspecialchars($trajet->getDepart()) . " -> " . htmlspecialchars($trajet->getArrivee()) . "<a href=\"?action=supprimer&controleur=trajet&id=" . rawurlencode($trajet->getId()) . "\"> supprimer</a>"  . " | " . "<a href=\"?action=afficherFormulaireMiseAJour&controleur=trajet&id=" . rawurlencode($trajet->getId()) . "\">modifier</a>" . "</p><br>";
    }
    echo "<a href=\"?action=afficherFormulaireCreation&controleur=trajet\">Créer un trajet</a>";
?>