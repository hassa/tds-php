<?php
    namespace App\Covoiturage\Controleur;
    use App\Covoiturage\Modele\DataObject\Trajet;
    use App\Covoiturage\Modele\DataObject\Utilisateur;
    use App\Covoiturage\Modele\Repository\TrajetRepository;
    use App\Covoiturage\Modele\Repository\UtilisateurRepository;
    use DateTime;

    class ControleurTrajet extends ControleurGenerique {

        public static function afficherListe(): void {
            $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
            self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);
        }

        public static function afficherDetail(): void {
            $erreur = 0;
            foreach ($_GET as $key => $value) {
                if ($key == "id") {
                    $erreur = 0;
                    $trajet = (new TrajetRepository())->recupererParClePrimaire($value);
                    if ($trajet == null) $erreur = 1;
                    else self::afficherVue("vueGenerale.php", ["trajet" => $trajet, "titre" => "Détail d'un trajet", "cheminCorpsVue" => "trajet/detail.php"]);
                } else $erreur = 1;
            }
            if ($erreur == 1) {
                $messageErreur = "Error trajet : " . $value . " not defined";
                self::afficherErreur($messageErreur);
            }
        }

        public static function afficherFormulaireCreation(): void
        {
            self::afficherVue("vueGenerale.php" , ["titre" => "Formulaire création trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
        }

        /**
         * @throws \DateMalformedStringException
         */
        public static function creerDepuisFormulaire(): void
        {
            $newtrajet = self::construireDepuisFormulaire($_GET);
            if ((new TrajetRepository())->ajouter($newtrajet)) {
                $trajets = (new TrajetRepository())->recuperer();
                self::afficherVue("vueGenerale.php", ["titre" => "trajet crée", "cheminCorpsVue" => "trajet/trajetCree.php", "trajets" => $trajets]);
            }
            else {
                self::afficherErreur("erreur: impossible de créer le trajet");
            }
        }

        public static function afficherFormulaireMiseAJour(): void {
            $id = $_GET['id'];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
            self::afficherVue("vueGenerale.php", ["titre" => "formulaire mise a jour", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
        }

        public static function mettreAJour(): void {
            $newtrajet = self::construireDepuisFormulaire($_GET);
            (new TrajetRepository())->mettreAJour($newtrajet);
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue("vueGenerale.php", ["titre" => "trajet mis a jour", "cheminCorpsVue" => "trajet/trajetMisAJour.php", "trajets" => $trajets]);
        }

        public static function supprimer(): void {
            $login = $_GET['id'];
            (new TrajetRepository())->supprimer($login);
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue("vueGenerale.php", ["titre" => "trajet supprimé", "cheminCorpsVue" => "trajet/trajetSupprime.php", "trajets" => $trajets]);
        }

        public static function afficherErreur(string $messageErreur): void {
            self::afficherVue("vueGenerale.php", ["titre" => "error page", "cheminCorpsVue" => "trajet/erreur.php", "messageErreur" => $messageErreur]);
        }



        /**
         * @return Trajet
         * @throws \DateMalformedStringException
         */
        public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
        {
            return new Trajet($tableauDonneesFormulaire['id'] ?? null, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], new DateTime($tableauDonneesFormulaire['date']), $tableauDonneesFormulaire['nbPlaces'], $tableauDonneesFormulaire['prix'], (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']), ($tableauDonneesFormulaire['nonFumeur'] == "on"));
        }

    }
?>