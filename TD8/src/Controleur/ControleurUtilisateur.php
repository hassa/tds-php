<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

use App\Covoiturage\Modele\HTTP\Cookie;


require_once __DIR__ . "/../Modele/DataObject/Utilisateur.php"; // chargement du modèle
class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();//appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetailConnecte() : void{

        $login =ConnexionUtilisateur::getLoginUtilisateurConnecte();

        $utilisateur =(new UtilisateurRepository())->recupererParClePrimaire($login);

        self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Detail de l'utilisateur connecté ", "cheminCorpsVue" => "utilisateur/detail.php"]);

    }

    public static function deposerCookie() : void{

        Cookie::enregistrer("test","valeur",time() + 3600);


    }

    public static function lireCookie() : void{

       Cookie::lire("test");

    }
    public static function deconnecter() :void{

        ConnexionUtilisateur::deconnecter();

        $utilisateurs= (new UtilisateurRepository())->recuperer();

        self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateurs, "titre" => "utilisateur deconnecté", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);



    }

    public static function connecter() :void{

        $login = $_GET['login'];

        $mdp =  $_GET['mdp'];

        if(!isset($login)){

            self::afficherErreur("Veuillez renseigner un login");
        }
        else if(!isset($mdp)){
            self::afficherErreur("Veuillez renseigner un mot de passe ");
        }

        else{

            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if($utilisateur){

                $mdphache= $utilisateur->getMdpHache();

                if(!(MotDePasse::verifier($mdp,$mdphache))){
                    self::afficherErreur(" mot de passe incorrect");
                }
                else{

                    ConnexionUtilisateur::connecter($login);
                    self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "utilisateur connecté", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"]);
                }
            }
            else{
                self::afficherErreur("Login  incorrect");
            }


        }
    }


    public static function afficherDetail(): void
    {
        $erreur = 0;
        foreach ($_GET as $key => $value) {
            if ($key == "login") {
                $erreur = 0;
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($value);
                if ($utilisateur == null) $erreur = 1;
                else self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Détail d'un utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
            } else $erreur = 1;
        }
        if ($erreur == 1) {
            $messageErreur = "Error user : " . $value . " not defined";
            self::afficherErreur($messageErreur);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php" , ["titre" => "Formulaire création utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {

        $mdp1= $_GET['mdp'];
        $mdp2= $_GET['mdp2'];

        $newutilisateur = self::construireDepuisFormulaire($_GET);
        if ((new UtilisateurRepository)->ajouter($newutilisateur)) {
            if($mdp2==$mdp1) {
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue("vueGenerale.php", ["titre" => "utilisateur crée", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
            }
        }
        else {
            self::afficherErreur("erreur: impossible de créer l'utilisateur");
        }
    }

    public static function supprimer(): void {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["titre" => "utilisateur supprimé", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "utilisateurs" => $utilisateurs]);
    }

    public static function afficherFormulaireMiseAJour(): void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue("vueGenerale.php", ["titre" => "formulaire mise a jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
    }

    public static function afficherFormulaireConnexion():void{


        self::afficherVue("vueGenerale.php" , ["titre" => "Formulaire connexion utilisateur", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);



    }

    public static function mettreAJour(): void {
        // Vérifier si les champs obligatoires sont transmis
        if (empty($_GET['login']) || empty($_GET['ancienMotDePasse']) || empty($_GET['nouveauMotDePasse']) || empty($_GET['confirmationMotDePasse'])) {
            self::afficherErreur("Tous les champs obligatoires doivent être remplis.");
            return;
        }

        // Vérifier que l'utilisateur connecté correspond à l'utilisateur mis à jour
        session_start();
        if (!ConnexionUtilisateur::estUtilisateur($_GET['login'])) {
            self::afficherErreur("Vous ne pouvez mettre à jour que votre propre compte.");
            return;
        }

        // Récupérer l'utilisateur à partir du login

        $utilisateur = $_GET['login'];

        if (!$utilisateur ) {
            self::afficherErreur("Le login spécifié n'existe pas.");
            return;
        }

        // Vérifier que l'ancien mot de passe est correct
        if (!password_verify($_GET['ancienMotDePasse'], $utilisateur->getMotDePasse())) {
            self::afficherErreur("L'ancien mot de passe est incorrect.");
            return;
        }

        // Vérifier que les deux nouveaux mots de passe coïncident
        if ($_GET['nouveauMotDePasse'] !== $_GET['confirmationMotDePasse']) {
            self::afficherErreur("Les nouveaux mots de passe ne correspondent pas.");
            return;
        }


    }

    public static function afficherErreur(string $messageErreur): void {
        self::afficherVue("vueGenerale.php", ["titre" => "error page", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }



    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $mdp= $tableauDonneesFormulaire['mdp'];
        $mdphach=  MotDePasse::hacher($mdp);
        $rep =  new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom'],$mdphach);


        return $rep;
    }

}

?>