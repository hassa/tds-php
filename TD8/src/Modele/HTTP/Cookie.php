<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{


    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{

        if($dureeExpiration){

            setcookie($cle, serialize($valeur), $dureeExpiration);
        }

    else{

        setcookie($cle, serialize($valeur),0);
    }
    }


    public static function lire(string $cle): mixed{

        $cookie = $_COOKIE[$cle];

        if($cookie){
            echo $cookie;
        }
        else{
            echo ("pas de cookie");
        }


        return $cookie;
    }


    public static function contient(string $cle): bool{


        return isset($_COOKIE[$cle]);

    }


    public static function supprimer(string $cle): void{



        unset($_COOKIE[$cle]);

        setcookie($cle, "supprim", 1);
    }





}

