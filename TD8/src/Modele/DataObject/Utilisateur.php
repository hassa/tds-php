<?php
namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class Utilisateur extends AbstractDataObject {
    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;
    private string $mdpHache;




    public function __construct(string $login,  string $prenom, string $nom, string $mdpHache)
    {
        $this->login = $login;
        $this->mdpHache = $mdpHache;
        //$this->trajetsCommePassager = $trajetsCommePassager;
        $this->prenom = $prenom;
        $this->nom = $nom;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }


    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    // un setter
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin($login): void
    {
        $this->login = substr($login, 0, 64);
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null) {
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }




    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString () : string {
        return $this->login . " - " . $this->nom . " - " . $this->prenom;
    }
}

?>

