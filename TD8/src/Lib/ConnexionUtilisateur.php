<?php

namespace App\Covoiturage\Lib;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }


        $_SESSION[self::$cleConnexion] = $loginUtilisateur;     }



    public static function estUtilisateur($login): bool{


            if (isset($_SESSION['login'])) {
                return $_SESSION['login'] === $login;
            }
            return false;

    }



    public static function estConnecte(): bool
    {
        return isset($_SESSION[self::$cleConnexion]);    }

    public static function deconnecter(): void
    {
        unset($_SESSION[self::$cleConnexion]);    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return $_SESSION[self::$cleConnexion];
    }
}

