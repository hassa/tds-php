<?php
// On inclut les fichiers de classe PHP pour pouvoir se servir de la classe ConfigurationBaseDeDonnees.
// require_once évite que ConfigurationBaseDeDonnees.php soit inclus plusieurs fois,
// et donc que la classe ConfigurationBaseDeDonnees soit déclaré plus d'une fois.
require_once 'ConfigurationBaseDeDonnees.php';

// On affiche le login de la base de donnees
echo "Nom de l'Hote : ".ConfigurationBaseDeDonnees::getNomHote().
    "<br>Nom de la Base : ".ConfigurationBaseDeDonnees::getNomBaseDeDonnees().
    "<br>Numero du port : ".ConfigurationBaseDeDonnees::getPort().
    "<br>Login : ".ConfigurationBaseDeDonnees::getLogin().
    "<br>Mot de passe : ".ConfigurationBaseDeDonnees::getMotDePasse()

?>
