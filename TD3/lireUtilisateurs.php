<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

echo "<h3>Liste des utilisateurs <h4>1/2</h4></h3>";

$mhm = new ConnexionBaseDeDonnees();
$pdoStatement = $mhm->getPdo()->query("SELECT * FROM utilisateur");

foreach ($pdoStatement as $utilisateurFormatTableau) {

    $utilisateur = new Utilisateur(
        $utilisateurFormatTableau['loginBaseDeDonnees'],
        $utilisateurFormatTableau['prenomBaseDeDonnees'],
        $utilisateurFormatTableau['nomBaseDeDonnees']
    );
    echo "$utilisateur <br>";
}

echo "<br><h3>Liste des utilisateurs <h4>2/2</h4></h3><br>";

$utilisateurs = Utilisateur::recupererUtilisateurs();
foreach ($utilisateurs as $utilisateur) {
    $utilisateur->afficher();
    echo "<br>";
}




