<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    /**
     * @var Utilisateur[]
     */
    private array $passagers;

    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur,
        array $passagers = []
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }


    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet {
        $date = new DateTime($trajetTableau['date']);
        $conducteur = Utilisateur::recupererUtilisateurParLogin($trajetTableau['conducteurLogin']);

        $trajet = new Trajet(
            $trajetTableau['id'],
            $trajetTableau['depart'],
            $trajetTableau['arrivee'],
            $date,
            $trajetTableau['prix'],
            $conducteur,
            (bool)$trajetTableau['nonFumeur']
        );

        $trajet->setPassagers($trajet->recupererPassagers());

        return $trajet;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter(): void {
        $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) 
            VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $stmt = $pdo->prepare($sql);

        $values = [
            ':depart' => $this->depart,
            ':arrivee' => $this->arrivee,
            ':date' => $this->date->format('Y-m-d'),
            ':prix' => $this->prix,
            ':conducteurLogin' => $this->conducteur->getLogin(),
            ':nonFumeur' => $this->nonFumeur ? 1 : 0
        ];

        $stmt->execute($values);

        echo "Le trajet a été ajouté avec succès dans la base de données.";
    }

    public function afficher() {
        echo $this->__toString();
    }

    private function recupererPassagers() : array {
        $pdo = ConnexionBaseDeDonnees::getPDO();

        $sql = "SELECT u.* 
            FROM utilisateur u
            INNER JOIN passager p ON u.login = p.passagerLogin
            WHERE p.trajetId = :trajetId";

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':trajetId', $this->id, PDO::PARAM_INT);

        $stmt->execute();

        $resultats = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $passagers = [];
        foreach ($resultats as $row) {
            $passagers[] = Utilisateur::construireDepuisTableauSQL($row);
        }

        return $passagers;
    }

}
