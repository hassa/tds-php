<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

echo "<h3>Exercice 1</h3>";

$log1 = "doublets";
$log2 = "doublet";

$utilisateurLogin = Utilisateur::recupererUtilisateurParLogin($log1);
if ($utilisateurLogin !== null) {
    $utilisateurLogin->afficher();
} else {
    echo "Aucun utilisateur trouvé pour le login $log1.<br>";
}

echo "<br>";

$utilisateurLogin = Utilisateur::recupererUtilisateurParLogin($log2);
if ($utilisateurLogin !== null) {
    $utilisateurLogin->afficher();
} else {
    echo "Aucun utilisateur trouvé pour le login $log2.<br>";
}

echo "<h3>Exercice 2</h3><br>";

$utilisateur = new Utilisateur('iron.man','man','iron');
$utilisateur->afficher();

//$utilisateur->ajouter();

$utilisateurs = Utilisateur::recupererUtilisateurs();
foreach ($utilisateurs as $utilisateur) {
    $utilisateur->afficher();
    echo "<br>";
}






