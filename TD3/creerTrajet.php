<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';
require_once 'Utilisateur.php';

$depart = $_POST['depart'];
$arrivee = $_POST['arrivee'];
$dateString = $_POST['date'];
$prix = (int) $_POST['prix'];
$conducteurLogin = $_POST['conducteurLogin'];
$nonFumeur = isset($_POST['nonFumeur']) ? true : false;

echo "<h3>Informations sur le trajet :</h3>";

echo "Depart : $depart<br>";
echo "Arrivee : $arrivee <br>";
echo "Date: $dateString <br>";
echo "Prix : $prix <br>";
echo "Conducteurs Login : $conducteurLogin <br>";
echo "Non Fumeur : " . ($nonFumeur ? 'Oui' : 'Non') . "<br>";

$date = new DateTime($dateString);

$conducteur = Utilisateur::recupererUtilisateurParLogin($conducteurLogin);

if ($conducteur === null) {
    echo "Erreur : Le conducteur avec le login $conducteurLogin n'existe pas.";
    exit;
}

$trajet = new Trajet(null, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
$trajet->afficher();
$trajet->ajouter();

echo "Le trajet a été créé et ajouté à la base de données.";
?>
