<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue("utilisateur/liste.php", $utilisateurs);  //"redirige" vers la vue
    }


    public static function afficherDetail() : void
    {
        $util= [];
        $login = $_GET['login'];
        if (empty($login)) {

            self::afficherVue("utilisateur/erreur.php", $util);
        } else {


            $utilisateurs = ModeleUtilisateur::recupererUtilisateurParLogin($login);
            if (!$utilisateurs) {

                self::afficherVue("utilisateur/erreur.php", $utilisateurs);
            }
            else {


                self::afficherVue("utilisateur/detail.php", $utilisateurs);
            }
        }
    }

    public static function afficherFormulaireCreation(): void{


        self::afficherVue("utilisateur/formulairecreation.php", []);
    }

    public static function creerDepuisFormulaire() : void{

        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];

        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);

        $utilisateur->ajouter();

    }


    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }



}
?>
