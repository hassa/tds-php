<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Afficher détail d'un utilisateur </title>
</head>
<body>

<?php
use App\Covoiturage\Modele\Utilisateur as ModeleUtilisateur;

$login = $_POST['login'];
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];

echo "<h3>Informations du nouvel utilisateur :</h3>";

echo htmlspecialchars("Login: $login ");
echo "<br>";
echo htmlspecialchars("Nom: $nom ");
echo "<br>";
echo htmlspecialchars("Prénom: $prenom");
echo "<br>";

$utilisateur = new Utilisateur($login, $nom, $prenom);


$utilisateur->ajouter();

?>

</body>
</html>
