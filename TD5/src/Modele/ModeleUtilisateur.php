<?php

namespace App\Covoiturage\Modele;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as  ConnexionBaseDeDonnees;
class ModeleUtilisateur {

    // Déclaration de type pour les attributs
    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(string $login, string $nom, string $prenom) {
        // Tronquer le login à 64 caractères
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getLogin(): string {
        return $this->login;
    }

    public function setLogin(string $login): void {
        // Tronquer le login à 64 caractères
        $this->login = substr($login, 0, 64);
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setNom(string $nom): void {
        $this->nom = $nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void {
        $this->prenom = $prenom;
    }

   /* public function __toString(): string {
        return "Utilisateur: $this->prenom $this->nom (Login: $this->login)";
    }*/

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function recupererUtilisateurs() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();

        $requete = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($requete);

        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public function afficher() {
        echo $this->__toString();
    }

    public static function recupererUtilisateurParLogin(string $login): ?Utilisateur {
        $sql = "SELECT * FROM utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );

        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        $utilisateurFormatTableau = $pdoStatement->fetch();

        // Si aucun résultat, retourner null
        if ($utilisateurFormatTableau === false) {
            return null;
        }

        // Sinon, on construit l'objet Utilisateur
        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): void {
        $sql = "INSERT INTO utilisateur (login, prenom, nom) VALUES (:value1, :value2, :value3)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "value1" => $this->getLogin(),
            "value2" => $this->getNom(),
            "value3" => $this->getPrenom(),
        );

        $pdoStatement->execute($values);

        echo '<br>Ajout de l\'utilisateur : ';
        echo htmlspecialchars($this->getLogin());
        echo ' effectué avec succès.<br>';

    }



}
?>


<?php