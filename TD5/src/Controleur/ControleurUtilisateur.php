<?php
//require_once __DIR__.'/../Modele/ModeleUtilisateur.php'; // chargement du modèle
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Utilisateur;
class  ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ['utilisateurs' =>$utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }


    public static function afficherDetail() : void
    {

        $login = isset($_GET['login']) ? $_GET['login'] : null;
        if ($login){



            $utilisateurs = Utilisateur::recupererUtilisateurParLogin($login);
            if($utilisateurs) {

                self::afficherVue("vueGenerale.php", ['utilisateurs' => $utilisateurs, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue
            }
            else {

                self::afficherVue("vueGenerale.php", ["titre" => "Erreur login", "cheminCorpsVue" => "utilisateur/erreur.php"]);  //"redirige" vers la vue
            }

        }

        else{
                self::afficherVue("vueGenerale.php", [ "titre" => "Erreur login", "cheminCorpsVue" => "utilisateur/erreur.php"]);  //"redirige" vers la vue
        }
    }

    public static function creerutilisateur():void
    {
        self::afficherVue("vueGenerale.php", [ "titre" => "Creer utilisateur", "cheminCorpsVue" => "utilisateur/creerutilisateur.php"]);  //"redirige" vers la vue
    }

    public static function afficherFormulaireCreation(): void{


        self::afficherVue("vueGenerale.php", ["titre" => "Affichage formulaire creation ", "cheminCorpsVue" => "utilisateur/formulairecreation.php"]);  //"redirige" vers la vue
    }




    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../../vue/$cheminVue"; // Charge la vue
    }



}
?>
