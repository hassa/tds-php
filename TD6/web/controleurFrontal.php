<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';
    // initialisation en activant l'affichage de débogage
    $chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
    $chargeurDeClasse->register();
    // enregistrement d'une association "espace de nom" → "dossier"
    $chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');
    // On récupère l'action passée dans l'URL
    $controleur = "utilisateur";
    $action = "afficherListe";
    if (isset($_GET['controleur'])) {
        $controleur = $_GET['controleur'];
    }
    $nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);
    if (class_exists($nomDeClasseControleur)) {
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            if (!in_array($action, get_class_methods(ControleurUtilisateur::class))) {
                $messageErreur = "Error : " . $action . " not defined";
                $nomDeClasseControleur::afficherErreur("no action set");
            }
            else $nomDeClasseControleur::$action();
        }
        else $nomDeClasseControleur::$action();
    }
    else ControleurUtilisateur::afficherErreur("no controleur set");
    // Appel de la méthode statique $action de ControleurUtilisateur
?>
