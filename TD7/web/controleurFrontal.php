<?php

// Chargement de l'autoloader
use App\Covoiturage\Controleur\ControleurGenerique;
use App\Covoiturage\Lib\PreferenceControleur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// Initialisation de l'autoloader
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// Récupération de l'action à effectuer (par défaut : afficherListe)
$action = $_GET['action'] ?? 'afficherListe';

// Récupération du contrôleur à utiliser
// On vérifie d'abord si une préférence de contrôleur est définie, sinon on prend celui de l'URL, sinon 'utilisateur' par défaut.
$controleur = PreferenceControleur::existe() ? PreferenceControleur::lire() : ($_GET['controleur'] ?? 'utilisateur');

// Construction du nom complet de la classe contrôleur
$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

// Vérification de l'existence de la classe du contrôleur
if (class_exists($nomDeClasseControleur)) {
    // Vérification de l'existence de l'action dans le contrôleur
    if (method_exists($nomDeClasseControleur, $action)) {
        // Appel dynamique de l'action du contrôleur
        $nomDeClasseControleur::$action();
    } else {
        // Si l'action n'existe pas, on utilise la méthode afficherErreur du contrôleur générique
        echo("erreur");
    }
} else {
    // Si le contrôleur n'existe pas, on utilise la méthode afficherErreur du contrôleur générique
    echo("erreur");
}
