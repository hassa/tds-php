<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\PreferenceControleur;
class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    private static function afficherFormulairePreference(): void{

        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire preference ", "cheminCorpsVue" => "formulairePreference.php"  ]);
    }



    private static function enregistrerPreference(): void{

        $val = $_POST['controleur_defaut'];

        PreferenceControleur::enregistrer($val);

        self::afficherVue("vueGenerale.php", ["titre" => "Préférence controleur enregistré ", "cheminCorpsVue" => "preferenceEnregistre.php", "valeur" => $val ]);
    }



}